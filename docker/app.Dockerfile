FROM moodlehq/moodle-php-apache:7.2

# Installing Xdebug php extension for debugging/profiling.
RUN apt-get update && \
	pecl install -f xdebug && \
	docker-php-ext-enable xdebug && \
	apt-get install net-tools
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
EXPOSE 9000
